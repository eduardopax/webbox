FROM openjdk:21
ADD build/libs/webbox-0.0.1.jar /application.jar
ENV JAVA_OPTS="-Xms64m -Xmx256m"
ENTRYPOINT java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /application.jar