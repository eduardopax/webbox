package com.webbox.component;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component("webbox")
public class HealthIndicatorComponent extends AbstractHealthIndicator {

    private static final Logger logger = LoggerFactory.getLogger(HealthIndicatorComponent.class);

    @Value("${webbox.time.startup:0}") private int timeToStartup;

    private boolean healthy = true;

    private LocalDateTime startupTime;

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

    @PostConstruct
    public void init() {
        logger.info("Health will be UP after {} seconds", timeToStartup);
        startupTime = LocalDateTime.now().plusSeconds(timeToStartup);
    }


    @Override
    protected void doHealthCheck(Health.Builder health) {

        if (isStartupTimeReached() && healthy) {
            logger.info("Health check with status UP");
            health.up();
        } else {
            logger.info("Health check with status DOWN");
            health.down();
        }
    }

    private boolean isStartupTimeReached() {
        return LocalDateTime.now().compareTo(startupTime) > 0;
    }
}
