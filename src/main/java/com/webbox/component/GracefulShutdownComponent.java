package com.webbox.component;

import org.apache.catalina.connector.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

@Component
public class GracefulShutdownComponent implements TomcatConnectorCustomizer, ApplicationListener<ContextClosedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(GracefulShutdownComponent.class);

    @Value("${webbox.time.shutdown:5}") private int timeToShutdown;

    private volatile Connector connector;

    @Override
    public void customize(Connector connector) {
        this.connector = connector;
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        logMethodCalled();
        logger.info("Waiting before Shutting down {} seconds!", timeToShutdown);
        // this.connector.pause();

        try {
            Thread.sleep(timeToShutdown * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("Shutting down application!");
    }

    private void logMethodCalled() {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        logger.trace("Calling [ {} ]", methodName);
    }

}
