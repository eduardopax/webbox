package com.webbox.controller;

import java.io.FileInputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.GitProperties;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("property")
public class PropertyController {

    private static final Logger logger = LoggerFactory.getLogger(PropertyController.class);

    @Value( "${webbox.file-path}" )
    private String filePath;

    @Autowired
    private GitProperties gitProperties;

    @Autowired
    private Environment environment;

    @GetMapping("/memory/{key}")
    public String getPropertyFromMemory(@PathVariable("key") String key) {

        logMethodCalled();
        try {
            return String.format("Property key [ %s ] : value [ %s ] ", key, environment.getProperty(key));
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    @GetMapping("/file/{filename}/{key}")
    public String getPropertyFromFile(@PathVariable("filename") String filename, @PathVariable("key") String key) {

        logMethodCalled();
        try {

            String path = filePath + filename;
            Properties properties = new Properties();
            properties.load(new FileInputStream(path));

            return String.format("Property file [ %s ] : key [ %s ] : value [ %s ] ", path, key, properties.getProperty(key));
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    private void logMethodCalled() {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        logger.trace("Calling [ {} ]", methodName);
    }
}
