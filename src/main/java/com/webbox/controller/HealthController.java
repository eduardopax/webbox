package com.webbox.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.webbox.component.HealthIndicatorComponent;

@RestController
public class HealthController {

    private static final Logger logger = LoggerFactory.getLogger(HealthController.class);

    @Autowired
    private HealthIndicatorComponent healthIndicator;

    @PostMapping("/healthy")
    public void healthy() {
        logMethodCalled();
        healthIndicator.setHealthy(true);
    }

    @PostMapping("/unhealthy")
    public void unhealthy() {
        logMethodCalled();
        healthIndicator.setHealthy(false);
    }

    private void logMethodCalled() {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        logger.trace("Calling [ {} ]", methodName);
    }
}
