package com.webbox.controller;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("exp")
public class ZExperimentController {

    private static final Logger logger = LoggerFactory.getLogger(ZExperimentController.class);

    @GetMapping("/any/{value1}/{value2}")
    public String getAnyValue(@PathVariable("value1") String value1, @PathVariable("value2") String value2) {

        logMethodCalled();
        try {
            String response = String.format("Property value1 [ %s ] : value2 [ %s ] ", value1, value2);
            logger.info(response);
            return response;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    @GetMapping("/any/{value1}/hostname")
    public String getAnyAndHostname(@PathVariable("value1") String value1) {

        logMethodCalled();
        try {
            String response = String.format("Property value1 [ %s ] : Hostname : [ %s ] ", value1, InetAddress.getLocalHost().getHostName());
            logger.info(response);
            return response;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    private void logMethodCalled() {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        logger.trace("Calling [ {} ]", methodName);
    }
}
