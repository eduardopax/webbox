package com.webbox.controller;

import java.net.InetAddress;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.GitProperties;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.annotation.Timed;

@RestController
@Timed
public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired private GitProperties gitProperties;

    @Autowired private Environment environment;

    @GetMapping("/")
    public String getDefault() {

        logMethodCalled();
        try {
            return getVersion();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    @GetMapping("/hostname")
    public String getHostname() {

        logMethodCalled();
        try {
            return String.format("Hostname : [ %s ] ", InetAddress.getLocalHost().getHostName());
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    @GetMapping("/version")
    public String getVersion() {

        logMethodCalled();
        try {
            return String.format("Version : [ %s ] ", gitProperties.getBranch() + "-" + gitProperties.getShortCommitId());
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    @GetMapping("/with-delay/{delay}")
    public String getWithDelay(@PathVariable("delay") Long delay) {

        logMethodCalled();
        try {
            logger.info("Request received and waiting for {} seconds" , delay);
            try {
                Thread.sleep(delay * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("Returning request");
            return "Ok";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "ERROR";
        }
    }

    @GetMapping("/headers")
    public Map<String, String> getHeaders(@RequestHeader Map<String, String> headers) {

        logMethodCalled();

        return headers;

    }


    private void logMethodCalled() {
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        logger.trace("Calling [ {} ]", methodName);
    }
}
