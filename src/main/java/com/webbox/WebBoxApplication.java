package com.webbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;

import com.webbox.component.GracefulShutdownComponent;

@SpringBootApplication
public class WebBoxApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebBoxApplication.class, args);
    }

    @Bean
    public ConfigurableServletWebServerFactory webServerFactory(final GracefulShutdownComponent gracefulShutdown) {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(gracefulShutdown);
        return factory;
    }
}
